---
title: Contact
contactUsText: |-
  240 Kent Avenue
  Brooklyn, NY 11249
  (718) 384-2248
contactUsHeader: Contact Us
supportUsText: 'Contributions help sustain our work, please consider a gift today!'
supportUsButtonText: '<a href="https://secure.qgiv.com/for/norbronei/">Donate Now</a>'
supportUsHeader: Support Us
joinHeader: Join Us
joinText: >-
  Our work is powered by neighbors coming together to improve their community.
  Please subscribe to our mailing list to learn how to get involved.
facebookLink: 'https://facebook.com/northbrooklynneighbors'
twitterkLink: 'https://twitter.com/nbklynneighbors'
instagramLink: 'https://instagram.com/northbrooklynneighbors'
---
This is the page that stores the contact info.
