---
title: 'Soil Safety Basics: A Lead in Soils Workshop'
date: 2019-05-11T20:43:00.000Z
image: /img/addressing_lead_in_soil_main.jpg
eventdate: 2019-06-08T14:00:00.000Z
description: >-
  On Saturday, June 8th from 10:00 am to 11:30 am at 61 Franklin Street
  Community Garden (61 Franklin St., Brooklyn, New York 11222), join us for this
  90-minute interactive, introductory workshop designed to help participants
  learn about lead contamination in exposed soils.
rsvpLink: 'https://nbnlead3.eventbrite.com/'
---
On Saturday, June 8th from 10:00 am to 11:30 am at [61 Franklin Street Community Garden (61 Franklin St., Brooklyn, New York 11222)](https://goo.gl/maps/Tx3wRduVo1qnNCwp9), join us for this 90-minute interactive, introductory workshop designed to help participants learn about lead contamination in exposed soils. 

Bring a soil sample to the workshop to participate in our free testing program.

<https://nbnlead3.eventbrite.com/>

_This workshop is an activity of Strengthening Our Common Ground: Lead in Soils in Greenpoint, a project made possible with funding provided by the Office of the New York State Attorney General and the New York State Department of Environmental Conservation through the Greenpoint Community Environmental Fund._
