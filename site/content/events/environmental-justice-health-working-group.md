---
title: EJH Working Group
date: 2019-03-13T19:10:22.897Z
image: /img/nbn-ej-components-small.jpg
eventdate: 2019-03-27T22:30:00.000Z
description: >-
  On Wednesday, March 27th at 6:30 pm at Dupont Senior Center the Environmental
  Justice & Health Working Group will bring neighbors together to develop
  community-based strategies to combat issues of air pollution, legacy toxins,
  toxic sites, and trash & waste. Please join us to find out more and become
  part of the working group.
rsvpLink: 'https://www.facebook.com/events/660501761019534/'
---
On Wednesday, March 27th at 6:30 pm at Dupont Senior Center the Environmental Justice & Health Working Group will bring neighbors together to develop community-based strategies to combat issues of air pollution, legacy toxins, toxic sites, and trash & waste. Please join us to find out more and become part of the working group.
