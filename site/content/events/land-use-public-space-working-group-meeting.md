---
title: Land Use & Public Space Working Group Meeting
date: 2019-06-13T22:32:02.574Z
image: /img/lups-logo.png
eventdate: 2019-06-18T22:30:00.000Z
description: >-
  On Tuesday, June 18th from 6:30 pm to 8:30 pm the Land Use & Public Space
  Working Group will  bring together neighbors to develop and advance grassroots
  community planning that seeks to preserve the mixed-income and mixed-use
  character of the community. To find out more and become part of the working
  group, please join us.
rsvpLink: 'https://www.facebook.com/events/454585735087544/'
---
On Tuesday, June 18th  from 6:30 pm to 8:30 pm the Land Use & Public Space Working Group will  bring together neighbors to develop and advance grassroots community planning that seeks to preserve the mixed-income and mixed-use character of the community. To find out more and become part of the working group, please join us.

To RSVP, call 718-384-2248 or contact@northbrooklynneighbors.org
