---
title: 'NuHart Comment-athon #2'
date: 2018-10-11T17:15:53.791Z
image: >-
  /img/harte-and-co-factory-280-franklin-street-greenpoint-52014-duotone-blue.jpg
eventdate: 2018-11-13T17:00:53.792Z
description: >-
  We will help residents craft their comments on NYS DEC's Proposed Remedial
  Action Plan (PRAP) for the former NuHart Plastics site. Our technical
  assistance adviser will be on hard to provide further support.
rsvpLink: 'mailto:lael@northbrooklynneighbors.org'
---
Join us on Tuesday, November 13th from 3-5pm at the Dupont Senior Center (80 Dupont Street, Brooklyn, NY 11222) for an afternoon offering community feedback on the NuHart Plastics PRAP. Polish translation will be available.

We will help residents craft their comments on NYS DEC's Proposed Remedial Action Plan (PRAP) for the former NuHart Plastics site. Our technical assistance adviser will be on hard to provide further support.

[NuHart Superfund Site Update (Oct. 2018)](https://drive.google.com/file/d/1Sh9Bl_-1LVoz3Iz36DYee4NVquCYvXKa/view?usp=sharing)

[Sample Issues to Raise about the PRAP](https://drive.google.com/file/d/1R4gH-oy3AcMSoAOhRSBRIA3_Svjl-YgO/view?usp=sharing)
