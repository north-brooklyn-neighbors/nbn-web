---
title: NuHart Superfund Community Forum
date: 2019-07-02T21:05:02.823Z
image: >-
  /img/harte-and-co-factory-280-franklin-street-greenpoint-52014-duotone-blue.jpg
eventdate: 2019-07-29T22:30:02.823Z
description: >-
  The NYS DEC recently selected its remediation plan for the NuHart Superfund
  site. Learn about the Record of the Decision from our new technical advisors,
  Dr. Joseph Gardella and Dr. Alan Rabideau.


  On Monday, July 29 at 6:30 pm at the Polish & Slavic Center (176 Java Street)
  bring your questions and hear the latest about the future of the site.
rsvpLink: 'https://www.facebook.com/events/2291833847724731/'
---
The NYS DEC recently selected its remediation plan for the NuHart Superfund site. Learn about the Record of the Decision from our new technical advisors, Dr. Joseph Gardella and Dr. Alan Rabideau.

On Monday, July 29 at 6:30 pm at the Polish & Slavic Center (176 Java Street) bring your questions and hear the latest about the future of the site.
