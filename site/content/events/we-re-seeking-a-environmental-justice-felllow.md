---
title: We're Seeking an Environmental Justice Fellow
date: 2019-07-23T15:52:33.585Z
image: /img/join-us-image.jpg
eventdate: 2019-08-09T15:52:33.586Z
description: "We're looking for an Environmental Justice Fellow!\n\nThe Fellow will work closely with the Executive Director, staff and partners to fulfill commitments of Mapping Environmental (In)Justice project. A project designed focused on the the environmental and social landscape of North Brooklyn.\_"
rsvpLink: 'mailto:fellowship@northbrooklyneighbors.org'
---
We're looking for an Environmental Justice Fellow!

The Fellow will work closely with the Executive Director, staff and partners to fulfill commitments of Mapping Environmental (In)Justice project. A project designed focused on the the environmental and social landscape of North Brooklyn. 

This is a paid, part-time (24 hours per week), temporary fellowship beginning in September 2019 and will last eight/nine months (negotiable). The Fellow will receive a bi-weekly stipend. 

Applications due Wednesday, August 7, 2019.

[Read the full description](https://drive.google.com/open?id=1YGI_mMJ5d3DTuDaS6Bzz-L80IlkRJuyv).
