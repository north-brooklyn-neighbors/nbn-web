---
title: 'Jane''s Walk: From Industry to Luxury'
date: 2019-04-27T19:14:50.298Z
image: /img/jw.jpg
eventdate: 2019-05-04T15:00:00.000Z
description: >-
  On Saturday, May 4th tour surviving artifacts of Greenpoint (11:00 am) and
  Williamsburg's (3:00 pm) industrial past, see how the neighborhoods have been
  transformed in this century, and consider the environmental issues that lie
  beneath. 
rsvpLink: >-
  https://www.mas.org/events/from-industry-to-luxury-a-walk-through-williamsburgs-industrial-legacy/
---
Tour surviving artifacts of Greenpoint and Williamsburg's industrial past, see how the neighborhoods have been transformed in this century, and consider the environmental issues that lie beneath. Ward Dennis will lead a tour of Greenpoint and a separate tour of Williamsburg. 

Greenpoint Tour at 11:00 am 

<https://www.mas.org/events/from-industry-to-luxury-a-walk-through-greenpoints-industrial-legacy/>

Meeting Location: Entrance of Manhattan Avenue Park, Brooklyn, NY



Williamsburg Tour  at 3:00 pm

<https://www.mas.org/events/from-industry-to-luxury-a-walk-through-williamsburgs-industrial-legacy/>

Meeting Location: Outside of 214 N 11th Street, Brooklyn, NY
