---
title: 'Lead in Soils: An Advanced Workshop'
date: 2019-06-02T20:41:00.000Z
image: /img/seedlings-growth-childrens-hands-seed-3448883.jpg
eventdate: 2019-06-15T14:00:00.000Z
description: >
  On Saturday, June 15th from 10:00 am to 1:00 pm at the McCarren Park
  Demonstration Garden, join us for three-hour workshop features experts that
  will help interpret your soil test results and what the results mean for your
  outdoor space, a hands-on activity, and free 40-lb bags of compost for
  attendees. Snacks provided. 
rsvpLink: 'https://nbnlead4.eventbrite.com/'
---
On Saturday, June 15th from 10:00 am to 1:00 pm at the McCarren Park Demonstration Garden, join us for three-hour workshop features experts that will help interpret your soil test results and what the results mean for your outdoor space, a hands-on activity, and free 40-lb bags of compost for attendees. Snacks provided. 

Attendees should have participated in basic workshop. [Registration required](https://nbnlead4.eventbrite.com/). 

_This workshop is an activity of Strengthening Our Common Ground: Lead in Soils in Greenpoint, a project made possible with funding provided by the Office of the New York State Attorney General and the New York State Dept. of Environmental Conservation through the Greenpoint Community Environmental Fund.
_
