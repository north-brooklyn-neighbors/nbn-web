---
title: 'Save the Date: 25th Anniversary Gala & Party'
date: 2019-08-18T23:39:14.713Z
image: /img/nbn-save-the-date-draft-5.png
eventdate: 2019-10-17T23:00:00.000Z
description: >-
  Please join us on Thursday, October 17th, at 7:00 p.m. at BIBA Williamsburg
  for our 25th Anniversary Gala & Party! We are celebrating our history activism
  and advocacy and the future of our work in Greenpoint and Williamsburg. 
rsvpLink: 'https://secure.qgiv.com/for/norbronei/event/805593/'
---
Please join us on Thursday, October 17th, at 7:00 p.m. at BIBA Williamsburg for our 25th Anniversary Gala & Party! We are celebrating our history activism and advocacy and the future of our work in Greenpoint and Williamsburg. 

[Sponsorships now available.](https://secure.qgiv.com/for/norbronei/event/805593/) Ticket sales starting soon.
