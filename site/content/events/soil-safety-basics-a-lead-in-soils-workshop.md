---
title: 'Soil Safety Basics: A Lead in Soils Workshop'
date: 2019-05-02T15:54:56.279Z
image: /img/addressing_lead_in_soil_main.jpg
eventdate: 2019-05-18T14:00:00.000Z
description: >-
  On Saturday, May 18th from 10am to 11:30am at the McCarren Park Demonstration
  Garden, join us for a workshop designed to help participants learn about lead
  contamination in exposed soils.
rsvpLink: >-
  https://www.eventbrite.com/e/soil-safety-basics-a-lead-in-soils-workshop-tickets-55525222435
---
On Saturday, May 11th  from 10am to 11:30am at McCarren Park Demonstration Garden, join us for this 90-minute interactive, introductory workshop designed to help participants learn about lead contamination in exposed soils. This workshop will detail simple and easy steps you, your family, and your neighbors can take to safely enjoy the outdoors. Bring a soil sample to the workshop to participate in our free testing program.

https://www.eventbrite.com/e/soil-safety-basics-a-lead-in-soils-workshop-tickets-55525222435

_This workshop is an activity of Strengthening Our Common Ground: Lead in Soils in Greenpoint, a project made possible with funding provided by the Office of the New York State Attorney General and the New York State Department of Environmental Conservation through the Greenpoint Community Environmental Fund._
