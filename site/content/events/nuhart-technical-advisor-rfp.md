---
title: Seeking Proposals for NuHart Technical Advisor
date: 2019-02-22T16:28:02.828Z
image: >-
  /img/harte-and-co-factory-280-franklin-street-greenpoint-52014-duotone-green.jpg
eventdate: 2019-03-23T03:30:00.000Z
description: >-
  We are looking for a new Technical Advisor for the NuHart Superfund site.
  Proposal deadline is Friday, March 22, 2019.
rsvpLink: 'mailto:anthony@northbrooklynneighbors.org'
---
We are looking for a new [Technical Advisor for the NuHart Superfund site](https://drive.google.com/file/d/1H2trmG6jubu0zP_c6HHua3tEL9_fTfpc/view?usp=sharing). Proposal deadline is Friday, March 22, 2019.
