---
title: 'Environmental Justice & Health Workshop Group Meeting #4'
date: 2019-07-02T20:49:32.732Z
image: /img/nbn-ej-components-small.jpg
eventdate: 2019-07-02T20:49:32.732Z
description: >-
  On Wednesday, July 17th at 6:30 pm at a location to be determined the
  Environmental Justice & Health Working Group will bring neighbors together to
  develop community-based strategies to combat issues of air pollution, legacy
  toxins, toxic sites, and trash & waste. Please join us to find out more and
  become part of the working group.
rsvpLink: 'https://www.facebook.com/events/480310702773215/'
---
On Wednesday, July 17th at 6:30 pm at a location to be determined the Environmental Justice & Health Working Group will bring neighbors together to develop community-based strategies to combat issues of air pollution, legacy toxins, toxic sites, and trash & waste. Please join us to find out more and become part of the working group.
