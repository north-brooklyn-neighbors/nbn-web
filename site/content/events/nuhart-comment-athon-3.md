---
title: 'NuHart Comment-athon #3'
date: 2018-10-11T17:19:18.714Z
image: >-
  /img/harte-and-co-factory-280-franklin-street-greenpoint-52014-duotone-green.jpg
eventdate: 2018-11-13T23:00:48.393Z
description: >-
  We will help residents craft their comments on NYS DEC's Proposed Remedial
  Action Plan (PRAP) for the former NuHart Plastics site. Our technical
  assistance adviser will be on hard to provide further support.
rsvpLink: 'mailto:lael@northbrookylnneighbors.org'
---
Join us on Tuesday, November 13th from 6-8pm at Barley (1025 Manhattan Avenue, Brooklyn) for an evening offering community feedback on the NuHart Plastics PRAP.

We will help residents craft their comments on NYS DEC's Proposed Remedial Action Plan (PRAP) for the former NuHart Plastics site. Our technical assistance adviser will be on hard to provide further support.

[NuHart Superfund Site Update (Oct. 2018)](https://drive.google.com/file/d/1Sh9Bl_-1LVoz3Iz36DYee4NVquCYvXKa/view?usp=sharing)

[Sample Issues to Raise about the PRAP](https://drive.google.com/file/d/1R4gH-oy3AcMSoAOhRSBRIA3_Svjl-YgO/view?usp=sharing)
