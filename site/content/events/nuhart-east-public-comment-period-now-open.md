---
title: NuHart East Public Comment Period Now Open
date: 2019-02-04T20:53:00.124Z
image: /img/nuhart-east.jpg
eventdate: 2019-02-04T20:53:00.125Z
description: >-
  NYS DEC is now accepting comments for the draft remediation plan for the
  NuHart East brownfield site. NuHart East is adjacent to the NuHart Plastics
  Manufacturing Superfund site. Comments accepted through April 1, 2019.
rsvpLink: 'mailto:yukyin.wong@dec.ny.gov'
---
The NYS Department of Environmental Conservation (DEC) is soliciting public comments for the [Brownfield application](https://drive.google.com/open?id=1HtLzi_W1DxXYIRCYwHgmET3SHuXUo7T1) and [draft remediation plan](https://drive.google.com/open?id=1Hp3Tv2F1c80MpJVFZqdCKuELXTi6FBSJ) for the NuHart East site. This is the property located just to the east of the NuHart Plastics Manufacturing Superfund site.

The site itself contains many of the same chemicals present at the nearby Superfund site, to a lesser extent. Contaminants of concern include volatile organic compounds (VOCs) and semi-volatile organic compounds (SVOCs) such as trichloroethylene (TCE), as well as phthalates as well as a variety of heavy metals. While more information on the danger to human and environmental health should be forthcoming, from the draft Remedial Action Work Plan there is “limited potential exposure to residents and commercial workers in adjacent buildings from dust or vapors during excavation of impacted soil.” In addition, there will be “continuous air monitoring during excavation activity.”

The proposed plan would remove all soils contaminated over certain standards and disposed of offsite. Because the property is intended to be redeveloped as a 6-story mixed-use commercial and residential building, this would likely entail excavating the entirety of the lot to a depth of 15 feet.

Acceptance into the Brownfield program may qualify the property for certain benefits such as liability release and tax credits.

Comments are due by April 1.

Comments can be submitted to the site Project Manager Bryan Wong at 47-40 21st Street, Long Island City, NY, 11101; via email at <mailto:yukyin.wong@dec.ny.gov> or by calling (716) 482-4905.
