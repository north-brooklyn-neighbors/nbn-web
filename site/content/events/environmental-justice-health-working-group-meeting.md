---
title: Environmental Justice & Health Working Group Meeting
date: 2019-02-01T16:46:30.522Z
image: /img/nbn-ej-components-small.jpg
eventdate: 2019-02-04T16:46:30.523Z
description: >-
  On Wednesday, February 13th at 6:30pm at Bushwick Inlet Park the 
  Environmental Justice & Health Working Group will bring neighbors together to
  develop community-based strategies to combat issues of air pollution, legacy
  toxins, toxic sites, and trash & waste. Please join us to find out more and
  become part of the working group.
rsvpLink: 'https://www.facebook.com/events/2198976323700083/'
---
On Wednesday, February 13th at 6:30pm at Bushwick Inlet Park the  Environmental Justice & Health Working Group will bring neighbors together to develop community-based strategies to combat issues of air pollution, legacy toxins, toxic sites, and trash & waste. Please join us to find out more and become part of the working group.
