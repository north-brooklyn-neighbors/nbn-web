---
title: 'NuHart Comment-athon #1'
date: 2018-10-11T16:14:41.087Z
image: >-
  /img/harte-and-co-factory-280-franklin-street-greenpoint-52014-duotone-green.jpg
eventdate: 2018-10-24T22:00:41.087Z
description: >-
  We will help residents craft their comments on NYS DEC's Proposed Remedial
  Action Plan (PRAP) for the former NuHart Plastics site. Our technical
  assistance adviser will be on hard to provide further support.
rsvpLink: 'mailto:lael@northbrooklynneighbors.org'
---
Join us on Wednesday, October 24 from 6-8pm at [Threes @ Franklin + Kent](https://www.threesbrewing.com/pages/greenpoint) ([113 Franklin Street](https://goo.gl/maps/AqDDwnbuvXA2)) for an evening to offer community feedback on the NuHart Plastics PRAP. 

We will help residents craft their comments on NYS DEC's Proposed Remedial Action Plan (PRAP) for the former NuHart Plastics site. Our technical assistance adviser will be on hard to provide further support.
