---
title: NBN & Partners Comment on the NuHart Clean-Up Plan
date: 2018-11-26T19:32:34.527Z
image: >-
  /img/harte-and-co-factory-280-franklin-street-greenpoint-52014-duotone-blue.jpg
eventdate: 2018-11-26T19:32:34.527Z
description: >-
  North Brooklyn Neighbors joined with seven partner groups to submit comments
  to the NYS DEC about its proposed clean up plan for the from NuHart
  Manufacturing site. 
rsvpLink: 'mailto:lael@northbrooklynneighbors.org'
---
[North Brooklyn Neighbors joined with seven partner groups to submit comments](https://drive.google.com/open?id=1FEGz7BSjJCThSKZEugYe3t5Cq09QqgPM) to the NYS DEC about its proposed clean up plan for the from NuHart Manufacturing site. 

The DEC's Record of Decision should be released in late December 2018 or early January 2019.
