---
title: Petroleum Vapors Town Hall
date: 2019-06-19T18:00:00.000Z
image: /img/petroleum-vapors-town-hall.jpg
eventdate: 2019-06-13T18:00:00.000Z
description: >-
  On Wednesday, June 12th North Brooklyn Neighbors hosted a community town hall.
  Read about the meeting by clicking "Read More." 
rsvpLink: 'https://www.facebook.com/events/2736397673043792/'
---
On Wednesday, June 12th North Brooklyn Neighbors hosted a community town hall. The town hall featured a moderated panel discussion and a Q&A with community residents.

**Panel: 
**Moderator - 
Rachel Spector, Director of the Environmental Justice Program -- New York Lawyers for the Public Interest

Rodney Rivera, Special Assistant -- NYS Dept. of Environmental Conservation (DEC)

Randall Austin, Chief, Spill Response Program, Region 2, Division of Environmental Remediation -- NYS Dept. of Environmental Conservation (DEC)

Mario Bruno, Assistant Commissioner, Intergovernmental Affairs --NYC Dept. of Environmental Protection (DEP)

Nathan Walz, Public Health Specialist III, Bureau of Toxic Substance Assessment,  
Exposure Characterization and Response Section -- NYS Dept. of Health (DOH)

Chris D'Andrea, Assistant Commissioner, Environmental Disease and Injury Prevention -- NYC Dept. of Health & Mental Hygiene (DOHMH)

**Summary: 
**This is the second public meeting hosted by North Brooklyn Neighbors (NBN) in response to the petroleum smells in Greenpoint. The first meeting on May 7th was attended by representatives from elected officials and the DEC. While work has been done to identify the source, it is as of now, still unidentified and residents are still reporting persistent petroleum smells both in their homes and on the street. 

At Wednesday’s town hall, North Brooklyn Neighbors identified three community demands:

1. We want identification of the source.
2. We want broad testing and mitigation for harms and/or damages that impact residents.
3. We want a real investigation of the wider Greenpoint (11222) community, accountability, and final report with findings and analysis of health risks.

**General information:
**DEC and DEP have been working together and have centered their investigation on the sewer system. The smells seem to be most intense at times of high sewer usage, such as mornings and evenings. Officials stated that the intermittent behavior of the smells suggests the issue lies within the sewer and is not due to vapor intrusion. According the state officials vapor intrusion usually continues at a steady rate throughout the day.

In general, they state that conditions are improving although the investigation is ongoing. DEC stated that in areas where the sewer has been “flushed” (cleaned), the smells have abated. The agencies are primarily concerned with smells within residences. 

The next step is to flush the Huron Street sewer. During the times when sewers have been flushed, there is generally increased vapors. They will work with elected officials to get the word out about when the Huron Street sewer will be flushed. They will monitor the sewers for vapors the day of the flush and the day after. 

The investigation so far as shown that the sewer itself appears to be sound and intact. In their effort to identify a source, agencies are looking into dumping, traffic accidents, or some other  one-time contamination event where petroleum entered the sewer system. 

The evacuation of the Greenpoint YMCA Early Childhood Center has not officially been linked to petroleum vapors. DEC was not alerted until the day after this event. The smells had abated and the location had been cleared by the fire department. There are conflicting reports of what the smell resembled. 

When reporting odors, make sure to distinguish between “gas” which is generally assumed to be “natural gas” and “gasoline.”

**McGolrick Park Odors:
**

The DEC has been out to areas near McGolrick Park to follow up on odor reports. The agency has not yet been able to record any vapor action in that area but has provided contact information to the people who have reported odors and asked that they continue to report issues. No further investigation is planned unless reports continue. 

**Health information
**

Health officials said that given the level of exposure, they do not foresee any health effects from exposure to the vapors.

_To discuss health concerns, here are some available resources:
_

NYC Dept. of Health & Mental Hygiene 24-hour hotline, ask for a toxicologist: 800-457-7362 or 646-632-6102. For a state toxicologist, call: 518-402-7800

There is no specific level at which that state decides to relocate someone. It is a combination of the presence of a strong smell and liquid contaminants in the building (which has not been found at any location in Greenpoint related to this issue). 

**
Items for further investigation, as identified by the community: 
**

* More robust air sampling in order to accurately assess the problem. Air sampling thus far has been very limited. This should include a greater number of samples taken as well as testing for a range of compounds. 
* Have high levels of development played a part in this issue coming to the forefront at this particular moment in time? This should examine both the disruption to the infrastructure from construction as well as additional residents using city infrastructure. 
* Are there linkages to odor reports in previous years? 
* There have been reports of a similar odor in the McGolrick Park area. Given that the sewer does not connect these two areas, how are these reports related to those in northern Greenpoint? 

**Agency next steps:
**

DEC: The agency shared that general readings from testing can be made public and they are willing to commit to periodic testing even after smells have abated. DEC also committed to continue to work with electeds and NBN to get word out to the public and will keep following up on leads. 

DEP: Agreed to continue to move forward with flushing the Huron St. sewer and other sewers as necessary.

Both City and State Health Depts will respond to complaints as necessary.
