---
title: North Brooklyn Neighbors
subtitle: Building a Better Community Since 1994
image: /img/nag-gwapp-are-now.png
pastProjectsLink: 'http://nag-brooklyn.org/issuesprojects/'
introduction:
  buttontext: Learn More
  headline: We're now North Brooklyn Neighbors
  text: >-
    Neighbors Allied for Good Growth and the Greenpoint Waterfront Alliance for
    Parks & Planning have joined forces to become North Brooklyn Neighbors.
section1:
  buttonlink: 'http://nag-brooklyn.org/issuesprojects/lead-education-toolkit'
  buttontext: Learn More
  headline: Addressing Lead in soil
  image: /img/addressing_lead_in_soil_main.jpg
  text: >-
    We work with Greenpoint residents to reduce lead exposure from past
    contamination by offering soil testing and workshops the emphasis safe
    gardening practices.
section2:
  buttonlink: 'http://nag-brooklyn.org/toxicity-map'
  buttontext: Explore the Map
  headline: Toxicity Map
  image: /img/toxicity_map_optimized.jpg
  text: >-
    This interactive map identifies toxic hot spots in Greenpoint and
    Williamsburg including sites that have already been remediated and sites
    that require future remediation.
whoweare:
  caption: 'NAG Brooklyn, 1994'
  image: /img/who_we_are_optimized.jpg
  textcol1: >-
    North Brooklyn Neighbors is a grassroots nonprofit organization serving the
    Greenpoint and Williamsburg sections of Brooklyn. Formed through the merger
    of Neighbors Allied for Good Growth (NAG) with the Greenpoint Waterfront
    Association for Parks & Planning (GWAPP), North Brooklyn Neighbors advances
    community-based solutions on issues of public space and the environment –
    through activism, education, and collaboration – to create a more just,
    healthy, and safe place to live and work.
  textcol2: >-
    Since 1994, North Brooklyn Neighbors (NBN) has made a difference in the
    lives of residents of Greenpoint and Williamsburg by reclaiming the
    waterfront, advocating increased access to open space, combating pollution
    and legacy toxins, promoting community-centered land use, and empowering
    neighbors with important resources. NBN envisions a North Brooklyn that
    actively collaborates to find solutions for neighborhood issues, is an
    effective climate and environmental justice advocate that prioritizes land
    use, public space, resiliency, and environmental health; and strengthens its
    diverse neighborhoods by providing resources, tools, and knowledge that
    promote positive change.
  title1: Who We Are
  title2: Our History
boardofficers:
  - name: Lisa Bloodgood
    title: Co-Chairperson
  - name: Nicole De Feo
    title: Co-Chairperson
  - name: Luke Ohlson
    title: Vice Chairperson
  - name: Alice Shay
    title: Secretary
  - name: Alan Minor
    title: Treasurer
boardmembers:
  - name: Caroline Bauer
  - name: Charlotte Binns
  - name: Ward Dennis
  - name: Roseann Henry
  - name: Felice Kirby
  - name: Ryan Kuonen
  - name: Mike Schade
  - name: Evan Thies
staffmembers:
  - link: ''
    name: Anthony Buissereth
    title: Executive Director
  - link: ''
    name: Lael Goodman
    title: Environmental Justice Program Manager
---

