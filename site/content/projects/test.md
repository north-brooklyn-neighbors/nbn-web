---
title: Clean Buses Healthy Ninos
date: 2018-10-01T18:11:34.016Z
image: /img/nbn-buses.jpg
description: >-
  We’re calling on New York State to invest in clean electric school buses
  especially in communities, like Greenpoint and Williamsburg, where the burden
  of breathing dirty air is higher.
tags: transportation
projectDate: 2018-10-01T18:11:34.031Z
projectLink: 'http://www.cleanride4kids.org/'
---
Volkswagen cheated federal emissions tests and polluted our air. They have agreed to pay over $127 million to settle civil and criminal suits in New York over their diesel emissions scandal.

Now we’re asking Governor Andrew Cuomo and decision makers at the Department of Environment Conservation to put our children’s health and safety first and use the $52.4 million allocated from VW settlement to fund electric school buses in communities most impacted like North Brooklyn.                              

Dirty diesel school buses harm children every day and contribute to high asthma rates. This will clean the air in our communities and protect the lungs of the thousands of kids who ride buses every day.
