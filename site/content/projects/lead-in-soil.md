---
title: Lead in Soils
date: 2018-09-19T22:21:34.094Z
image: /img/for-website.jpg
description: >+
  Strengthening Our Common Ground: Lead in Soils in Greenpoint is a local
  environmental education and stewardship project designed to benefit the
  Greenpoint public by helping to restore the neighborhood's soil. NBN is
  working to educate Greenpoint residents on ways to reduce human exposure to
  lead by implementing safe gardening practices in areas with the potential for
  elevated levels of lead in soil. 

tags: 'lead, Greenpoint, safety'
projectDate: 2018-09-19T22:21:34.100Z
projectLink: >-
  http://nag-brooklyn.org/issuesprojects/greenpoint-lead-in-garden-soil-outreach-project/
---
This project will help restore the soil in Greenpoint, thus improving public health. The project focuses on home and community gardens and young children most at risk to developmental issues as a result of high blood lead levels.

_Brochures:_

[Safety Basics](https://drive.google.com/file/d/1S7dxKI3NY-oNB7A-qp0bNvjy9ZYIOoZl/view?usp=sharing) 

[Greenpoint's Lead (Pb) History](https://drive.google.com/file/d/17Gdir1WptXSd5vl3y-uUQHSjpKpqojAc/view?usp=sharing)
