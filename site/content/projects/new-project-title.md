---
title: Nuhart Superfund Site
date: '2018-06-14T16:25:44-04:00'
image: /img/nbn-nuhart.jpg
description: >-
  In 2010, the NuHart property was classified by the state DEC as a Class 2
  state Superfund site, “representing a significant threat to public health
  and/or the environment and requiring action.”  The current owner of the
  property, Dupont Street Developers, LLC, plans to develop residential
  buildings at the site. Before development takes place, however, there will be
  a plan to address the industrial pollutants currently onsite.
tags: superfund
projectDate: '2017-07-06T16:25:44-04:00'
projectLink: 'http://nag-brooklyn.org/nuhart-superfund-site/'
---
At present, the site contains underground plumes of phthalates and trichloroethylene (TCE), chemicals remaining from the site’s prior usage as a vinyl plastics manufacturer, which discontinued operations in 2004. These chemicals, if not properly remediated, may pose serious hazards to human health and the environment.  



After the site’s current owner submits its site cleanup plan to the DEC, ESC will develop detailed analysis and recommendations, which will then be shared with NAG and the community.  ESC will also be developing factsheets on the site and will speak at upcoming community meetings about the site. Community members are invited to meet with ESC to discuss concerns about site cleanup.
